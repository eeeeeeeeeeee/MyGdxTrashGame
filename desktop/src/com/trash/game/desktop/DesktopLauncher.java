package com.trash.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.trash.game.MyGdxTrash;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "zds";
		config.width = 1024;
		config.height = 768;
		config.foregroundFPS = 62;
		config.backgroundFPS = 62;
		config.useGL30 = true;
		new LwjglApplication(new MyGdxTrash(), config);
	}
}
