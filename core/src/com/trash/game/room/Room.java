package com.trash.game.room;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.trash.game.ai.AIGrid;
import com.trash.game.units.Unit;
import com.trash.game.units.UnitFactory;

import java.util.ArrayList;

public class Room {
    public OrthographicCamera camera;
    private ArrayList<Layer> layers;
    public UnitFactory factory;
    public AIGrid aiGrid;

    public Room() {
        factory = new UnitFactory();
        aiGrid = new AIGrid();
    }

    public void initAI(int w, int h, int sizex, int sizey) {
        aiGrid.buildGrid(w, h, sizex, sizey, factory);
    }

    /*public boolean checkSolid(double x, double y) {
        if(factory.checkSolid(x, y) != null) return false;
        return true;
    }*/

    public boolean isPlaceFree(double x, double y) {
        return aiGrid.checkPlace((int)x, (int)y);
    }

    public void setCamera(OrthographicCamera camera) {
        this.camera = camera;
    }

    public void loadMap(String filename) {
        layers = RoomParser.importLayers(filename);
        factory = RoomParser.importObjects(filename);
    }

    public void update() {
        if(camera.position.x-camera.viewportWidth/2<0) camera.position.x = camera.viewportWidth/2;
        if(camera.position.y-camera.viewportHeight/2<0) camera.position.y = camera.viewportHeight/2;
        camera.update();
    }

    public void step() {
        if(layers!=null) {
            for(Layer layer : layers) {
                layer.draw();
            }
        }
        factory.step();
    }
}
