package com.trash.game.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class View {
    public int x, y, width, height;

    public View(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void update() {
        Gdx.gl.glClear(GL20.GL_VIEWPORT);
        Gdx.gl.glViewport(x, y, width, height);
    }
}
