package com.trash.game.room;

import com.trash.game.units.Unit;
import com.trash.game.units.UnitFactory;
import com.trash.game.units.collection.Player;
import com.trash.game.units.collection.Wall;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class RoomParser {
    private static Unit createObject(String type, int x, int y, int w, int h) {
        if(type.equals("player")) return new Player(x, y, w, h);
        if(type.equals("wall")) return new Wall(x, y, w, h);
        return null;
    }

    public static UnitFactory importObjects(String filename) {
        UnitFactory factory = new UnitFactory();
        JSONParser parser = new JSONParser();
        JSONObject object = null;
        try {
            object = (JSONObject) parser.parse(new FileReader(filename));
            JSONArray layers = (JSONArray) object.get("layers");
            for(int i=0;i<layers.size();i++) {
                JSONObject layer = (JSONObject)layers.get(i);
                String type = (String) layer.get("type");
                if(type.equals("objectgroup")) {
                    JSONArray objects = (JSONArray) layer.get("objects");
                    for(int j=0;j<objects.size();j++) {
                        JSONObject obj = (JSONObject)objects.get(j);
                        long x = (Long) obj.get("x");
                        long y = (Long) obj.get("y");
                        long width = (Long) obj.get("width");
                        long height = (Long) obj.get("height");
                        String tp = (String) obj.get("type");
                        Unit unit = createObject(tp, (int)x, (int)y, (int)width, (int)height);
                        if(unit!=null) factory.add(unit);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return factory;
    }

    public static ArrayList<Layer> importLayers(String filename) {
        ArrayList<Layer> layerslist = new ArrayList<Layer>();
        JSONParser parser = new JSONParser();
        JSONObject object = null;

        //PARSING IMAGEINFO
        HashMap<String, ImageInfo> images = new HashMap<String, ImageInfo>();
        try {
            object = (JSONObject) parser.parse(new FileReader(filename));
            JSONArray tilesets = (JSONArray) object.get("tilesets");
            for(int i=0;i<tilesets.size();i++) {
                JSONObject tileset = (JSONObject) tilesets.get(i);
                String image = (String) tileset.get("image");
                String name = (String) tileset.get("name");
                Long imagewidth = (Long) tileset.get("imagewidth");
                Long imageheight = (Long) tileset.get("imageheight");
                Long tilewidth = (Long) tileset.get("tilewidth");
                Long tileheight = (Long) tileset.get("tileheight");
                images.put(name, new ImageInfo(image, name, imagewidth, imageheight, tilewidth, tileheight));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // PARSING LAYERS
        try {
            object = (JSONObject) parser.parse(new FileReader(filename));
            JSONArray layers = (JSONArray) object.get("layers");
            for(int i=0;i<layers.size();i++) {
                JSONObject layer = (JSONObject)layers.get(i);
                String type = (String) layer.get("type");
                if(type.equals("tilelayer")) {
                    JSONArray data = (JSONArray) layer.get("data");
                    long height = (Long) layer.get("height");
                    long width = (Long) layer.get("width");
                    long[] _data = new long[data.size()];
                    for(int j=0;j<data.size();j++) {
                        _data[j] = (Long) data.get(j);
                    }
                    String name = (String) layer.get("name");
                    layerslist.add(new Layer(name, _data, width, height, images.get(name)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return layerslist;
    }
}
