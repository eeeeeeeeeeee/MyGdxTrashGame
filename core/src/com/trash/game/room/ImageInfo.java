package com.trash.game.room;

public class ImageInfo {
    public String image;
    public String name;
    public long imagewidth, imageheight;
    public long tilewidth, tileheight;

    public ImageInfo(String image, String name, long imagewidth, long imageheight, long tilewidth, long tileheight) {
        this.image = image;
        this.name = name;
        this.imagewidth = imagewidth;
        this.imageheight = imageheight;
        this.tilewidth = tilewidth;
        this.tileheight = tileheight;
    }
}