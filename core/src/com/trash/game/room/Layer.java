package com.trash.game.room;

import com.badlogic.gdx.graphics.Texture;
import com.trash.game.graphics.Sprite;

import static com.trash.game.MyGdxTrash.spriteBatch;

public class Layer {
    String name;
    Sprite data[][];
    long width;
    long height;
    ImageInfo imageInfo;
    Texture texture;

    public Layer(String name, long[] data, long width, long height, ImageInfo imageInfo) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.imageInfo = imageInfo;
        this.data = new Sprite[(int)width][(int)height];

        if(imageInfo==null) return;

        texture = new Texture(imageInfo.image);

        int w = (int) (imageInfo.imagewidth/imageInfo.tilewidth);

        for(int i=0;i<data.length;i++) {
            if(data[i] != 0) {
                int x = (int) (data[i] - 1) % w;
                int y = (int) (data[i] - 1) / w;
                this.data[i % (int) width][i / (int) width] = new Sprite(texture, (int)(x*imageInfo.tilewidth),
                        (int)(y*imageInfo.tileheight), (int)imageInfo.tilewidth, (int)imageInfo.tileheight);
                this.data[i % (int) width][i / (int) width].setPosition((i % (int) width)*(int)imageInfo.tilewidth,
                        (i / (int) width)*((int)imageInfo.tileheight));
            }
        }
    }

    public void draw() {
        for(int i=0;i<width;i++) {
            for(int j=0;j<height;j++) {
                Sprite sprite = data[i][j];
                if(sprite!=null) {
                    sprite.draw(spriteBatch);
                }
            }
        }
    }
}
