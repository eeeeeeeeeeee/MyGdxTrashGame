package com.trash.game.graphics;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Sprite extends com.badlogic.gdx.graphics.g2d.Sprite {
    public Sprite() {
    }

    public Sprite(Texture texture) {
        super(texture);
        flip(false, true);
    }

    public Sprite(Texture texture, int srcWidth, int srcHeight) {
        super(texture, srcWidth, srcHeight);
        flip(false, true);
    }

    public Sprite(Texture texture, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(texture, srcX, srcY, srcWidth, srcHeight);
        flip(false, true);
    }

    public Sprite(TextureRegion region) {
        super(region);
        flip(false, true);
    }

    public Sprite(TextureRegion region, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(region, srcX, srcY, srcWidth, srcHeight);
        flip(false, true);
    }

    public Sprite(com.badlogic.gdx.graphics.g2d.Sprite sprite) {
        super(sprite);
        flip(false, true);
    }
}
