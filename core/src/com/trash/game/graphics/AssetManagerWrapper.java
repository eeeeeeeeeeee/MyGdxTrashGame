package com.trash.game.graphics;

import com.badlogic.gdx.assets.AssetManager;

public class AssetManagerWrapper {
    AssetManager assetManager;

    public AssetManagerWrapper() {
        assetManager = new AssetManager();
    }

    public Object get(String file, Class cls) {
        if(!assetManager.isLoaded(file)) {
            assetManager.load(file, cls);
            assetManager.finishLoading();
        }
        return assetManager.get(file);
    }
}
