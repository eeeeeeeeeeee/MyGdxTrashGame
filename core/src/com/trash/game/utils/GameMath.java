package com.trash.game.utils;

public class GameMath {
    public static double pointDistance(double x, double y, double x1, double y1) {
        return Math.abs(Math.sqrt(x*x+y*y)-java.lang.Math.sqrt(x1*x1+y1*y1));
    }
    public static double pointDirection(double x, double y, double x1, double y1) {
        double xn = 0, yn = -1;
        double _x = x1-x, _y = y1-y;
        int cof = -1;
        if(x<x1) cof = 1;
        return Math.toDegrees(Math.acos((xn*_x+yn*_y) / (Math.sqrt(xn*xn+yn*yn) * java.lang.Math.sqrt(_x*_x+_y*_y))))*cof;
    }
}
