package com.trash.game.ai;

import com.badlogic.gdx.math.Vector2;
import com.trash.game.units.Unit;
import com.trash.game.units.UnitFactory;

public class AIGrid {
    private boolean grid[][];
    private int gridX, gridY, gridW, gridH;
    private int sizeX, sizeY;

    public void buildGrid(int x, int y, int w, int h, UnitFactory factory) {
        grid = new boolean[w][h];
        gridX = x;
        gridY = y;
        gridW = w;
        gridH = h;
        sizeX = w*x;
        sizeY = h*y;
        for(Unit unit : factory) {
            if(unit.solid) {
                try {
                    for(int i=(int)unit.position.x;i<unit.position.x+unit.size.x;i++) {
                        for(int j=(int)unit.position.y;j<unit.position.y+unit.size.y;j++) {
                            grid[i/x][j/y] = true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean gridAICheck(int x, int y, int[][] aigrid) {
        boolean of = true;
        return of;
    }

    public AIPath findPath(int x, int y, int x1, int y2) {
        AIPath path = new AIPath();
        int aigrid[][] = new int[gridW][gridH];

        return path;
    }

    public boolean checkPlace(int x, int y) {
        if(x<0||x>sizeX||y<0||y>sizeY) return false;
        return !grid[x/gridX][y/gridY];
    }
}
