package com.trash.game.units.command;

import com.badlogic.gdx.math.Vector2;

public class Action {
    public int type;
    public Vector2 params;

    public Action(int type, Vector2 params) {
        this.type = type;
        this.params = params;
    }
}
