package com.trash.game.units.collection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.trash.game.graphics.Sprite;
import com.trash.game.units.Unit;
import com.trash.game.units.command.Action;
import com.trash.game.units.command.ActionQuery;
import com.trash.game.utils.GameMath;

import static com.trash.game.MyGdxTrash.assetManager;
import static com.trash.game.MyGdxTrash.room;
import static com.trash.game.MyGdxTrash.spriteBatch;

public class Player extends Unit {
    private Sprite sprite;
    private double mspeed = 5;
    private double maxMspeed = 5;
    private double vspeed, vmax;
    private double hspeed = 0;
    private int jump = 15;
    private int lean = 0;
    private double animation_index = 0;
    private boolean flip = true;
    private int jumpTime = 0;
    private int maxJumpTime = 7;

    public Player(int x, int y, int w, int h) {
        super(x, y, w, h);
        Texture texture = (Texture) assetManager.get("player_platformerv2.png", Texture.class);
        sprite = new Sprite(texture, 0, 0, 16, 16);
        sprite.setOrigin(8,8);
        size.x = 16;
        size.y = 15;
        vspeed = 0;
        vmax = 7;
    }

    private boolean move_y(double dy) {
        double sz = -1;
        double dif = -1;
        if(dy>0) {
            sz = size.y;
            dif = 1;
        }
        double _dy = 0;
        if(dy>0) {
            while (_dy + dif < dy) {
                if (!room.isPlaceFree(position.x, position.y + sz + _dy + dif) ||
                        !room.isPlaceFree(position.x + size.x, position.y + sz + _dy + dif)) {
                    break;
                }
                _dy += dif;
            }
        } else {
            while (_dy + dif > dy) {
                if (!room.isPlaceFree(position.x, position.y + sz + _dy + dif) ||
                        !room.isPlaceFree(position.x + size.x, position.y + sz + _dy + dif)) {
                    break;
                }
                _dy += dif;
            }
        }
        position.y += _dy;

        return false;
    }

    private boolean move_x(double dx) {
        double sz = 0;
        double dif = -1;
        boolean touch = false;
        if(dx>0) {
            sz = size.x;
            dif = 1;
        }
        double _dx = 0;
        if(dx>0) {
            while (_dx + dif < dx) {
                if (!room.isPlaceFree(position.x + sz + _dx + dif, position.y) ||
                        !room.isPlaceFree(position.x + sz + _dx + dif, position.y + size.y)) {
                    touch = true;
                    break;
                }
                _dx += dif;
            }
        } else if(dx<0) {
            while (_dx + dif > dx) {
                if (!room.isPlaceFree(position.x + sz + _dx + dif, position.y) ||
                        !room.isPlaceFree(position.x + sz + _dx + dif, position.y + size.y)) {
                    touch = true;
                    break;
                }
                _dx += dif;
            }
        }
        if(!touch) {
            if (!room.isPlaceFree(position.x + dx + sz, position.y) ||
                    !room.isPlaceFree(position.x + dx + sz, position.y + size.y)) {
                touch = true;
            } else _dx = dx;
        } else hspeed = 0;

        position.x += _dx;

        if(touch&&room.isPlaceFree(position.x + sz, position.y + size.y + 1)&&room.isPlaceFree(position.x + sz, position.y - 1)) {
            if(dx>0) lean = 17; else lean = -17;
        }

        return false;
    }

    @Override
    public void step() {
        boolean moving = false;
        boolean falling = true;
        double gravity = 0.6;
        room.camera.position.x = position.x;
        room.camera.position.y = position.y;

        if(!room.isPlaceFree(position.x, position.y+size.y+1)||!room.isPlaceFree(position.x+size.x, position.y+size.y+1)) {
            if(jump<1) {
                falling = false;
                jumpTime = maxJumpTime;
                lean = 0;
                vspeed = 0;
            }
        } else {
            if(jumpTime>0) jumpTime -= 1;
            if(room.isPlaceFree(position.x+size.x+2, position.y)&&room.isPlaceFree(position.x-1, position.y)) {
                lean = 0;
            }
        }

        double running = 2;
        double friction = 1;
        double acceleration = 0.15;
        if(falling) {
            running = 0.7;
            friction = 0.25;
        }

        if(hspeed>0) {
            if(hspeed-friction<0) hspeed = 0; else hspeed -= friction;
        } else if(hspeed<0) {
            if(hspeed+friction>0) hspeed = 0; else hspeed += friction;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
            if(mspeed+acceleration<maxMspeed) mspeed += acceleration; else mspeed = maxMspeed;
            if(lean==0) {
                if(hspeed>-mspeed) {
                    if(hspeed-running>-mspeed) hspeed -= running; else hspeed = -mspeed;
                }
                moving = true;
            } else {
                if(lean>0) {
                    lean -= 1;
                }
            }
        } else if(Gdx.input.isKeyPressed(Input.Keys.D)) {
            if(mspeed+acceleration<maxMspeed) mspeed += acceleration; else mspeed = maxMspeed;
            if(lean==0) {
                if(hspeed<mspeed) {
                    if(hspeed+running<mspeed) hspeed += running; else hspeed = mspeed;
                }
                moving = true;
            } else {
                if(lean<0) {
                    lean += 1;
                }
            }
        } else {
            mspeed = 0;
        }

        if(hspeed!=0) {
            if(hspeed>0) flip = false; else flip = true;
            move_x(hspeed);
        }

        animation_index += 0.15;
        if(animation_index>=2) animation_index = 0;

        if(Gdx.input.isKeyPressed(Input.Keys.W)&&jump>0) {
            jump -= 1;
            vspeed -= 0.8;
        }

        if(lean!=0) {
            gravity = 0.5;
            if(lean>0) flip = true; else flip = false;
            sprite.setRegion(0,32,16,16);
        } else {
            if (falling) {
                sprite.setRegion(0, 48, 16, 16);
            } else {
                if (moving) {
                    sprite.setRegion(16 * (int) animation_index, 16, 16, 16);
                } else {
                    sprite.setRegion(0, 0, 16, 16);
                }
            }
        }

        if(vspeed+gravity<=vmax) vspeed+=gravity;
        move_y(vspeed);

        if(falling&&lean==0&&!room.isPlaceFree(position.x, position.y-2)) {
            sprite.setRegion(16 * (int) animation_index, 16, 16, 16);
            sprite.setFlip(flip, false);
        } else {
            sprite.setFlip(flip, true);
        }
        sprite.setPosition(position.x, position.y);
        sprite.draw(spriteBatch);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.W: {
                if(lean==0) {
                    double _dy = 0;
                    if(vspeed>0) _dy = vspeed;
                    if (jumpTime>0 || (!room.isPlaceFree(position.x, position.y + size.y + _dy)||
                            !room.isPlaceFree(position.x+size.x, position.y + size.y + _dy))) {
                        jump = 10;
                        vspeed = -6;
                    } else {
                        if (!room.isPlaceFree(position.x, position.y - 2)) {
                            if(Gdx.input.isKeyPressed(Input.Keys.D)) {
                                hspeed = 4;
                            }
                            if(Gdx.input.isKeyPressed(Input.Keys.A)) {
                                hspeed = -4;
                            }
                            vspeed = vmax;
                        }
                    }
                } else {
                    if(lean>0) {
                        jump = 10;
                        vspeed = -5;
                        hspeed = -8;
                    } else {
                        jump = 10;
                        vspeed = -5;
                        hspeed = 8;
                    }
                    lean = 0;
                }
                break;
            }
            case Input.Keys.A: {
                flip = true;
                break;
            }
            case Input.Keys.D: {
                flip = false;
                break;
            }
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        jump = 0;
        return super.keyUp(keycode);
    }
}
