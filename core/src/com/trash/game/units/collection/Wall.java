package com.trash.game.units.collection;

import com.badlogic.gdx.graphics.Colors;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.trash.game.units.Unit;

import static com.trash.game.MyGdxTrash.spriteBatch;

public class Wall extends Unit {
    public Wall(int x, int y, int w, int h) {
        super(x, y, w, h);
        solid = true;
    }

    @Override
    public void step() {
        super.step();
        /*spriteBatch.end();
        ShapeRenderer shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(spriteBatch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Colors.get("RED"));
        shapeRenderer.rect(position.x, position.y, size.x,  size.y);
        shapeRenderer.end();
        spriteBatch.begin();*/
    }
}
