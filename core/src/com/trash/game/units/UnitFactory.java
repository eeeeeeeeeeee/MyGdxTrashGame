package com.trash.game.units;

import com.badlogic.gdx.InputProcessor;

import java.util.ArrayList;

public class UnitFactory extends ArrayList<Unit> implements InputProcessor {
    public void step() {
        for(Unit unit : this) {
            unit.step();
        }
    }

    public Unit checkSolid(double x, double y) {
        for(Unit unit : this) {
            if(unit.solid && x>unit.position.x && x<unit.position.x+unit.size.x &&
                y>unit.position.y && y<unit.position.y+unit.size.y) {
                return unit;
            }
        }
        return null;
    }

    @Override
    public boolean keyDown(int keycode) {
        for(Unit unit : this) {
            unit.keyDown(keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        for(Unit unit : this) {
            unit.keyUp(keycode);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        for(Unit unit : this) {
            unit.touchDown(screenX, screenY, pointer, button);
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
