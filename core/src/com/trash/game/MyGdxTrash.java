package com.trash.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.trash.game.graphics.AssetManagerWrapper;
import com.trash.game.room.Layer;
import com.trash.game.room.Room;
import com.trash.game.room.RoomParser;
import com.trash.game.room.View;
import com.trash.game.units.UnitFactory;
import com.trash.game.units.collection.Player;

public class MyGdxTrash extends ApplicationAdapter {
    public static SpriteBatch spriteBatch;
    public static Room room;
    public static AssetManagerWrapper assetManager;

    @Override
    public void create () {
        assetManager = new AssetManagerWrapper();

        spriteBatch = new SpriteBatch();
        //factory = new UnitFactory();
        //factory.add(new Player(new Texture("player.png")));
        //view = new View(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        /*camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        layer = RoomParser.importLayers("map.json").get(0);*/
        OrthographicCamera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(true, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        room = new Room();
        room.setCamera(camera);
        room.loadMap("map16x16v2.json");
        room.initAI(16, 16, 100, 100);
        Gdx.input.setInputProcessor(room.factory);

        ShaderProgram shaderProgram = new ShaderProgram(Gdx.files.internal("shaders/default.vert"),
                Gdx.files.internal("shaders/default.frag"));
        spriteBatch.setShader(shaderProgram);
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(37f/255, 37f/255, 37f/255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        room.update();
        spriteBatch.setProjectionMatrix(room.camera.combined);
        spriteBatch.begin();
        room.step();
        spriteBatch.end();
        spriteBatch.flush();
    }

    @Override
    public void dispose () {
        spriteBatch.dispose();

    }
}
